package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member yellow_kirby = new OrdinaryMember("yellow_kirby","paladdin");
		guild.addMember(guildMaster, yellow_kirby);
    }

    @Test
    public void testMethodRemoveMember() {
        Member yellow_kirby = new OrdinaryMember("yellow_kirby","paladdin");
		guild.addMember(guildMaster, yellow_kirby);
		Member isthatyellowkirby = guild.getMember("yellow_kirby","paladdin");
		guild.removeMember(guildMaster,isthatyellowkirby);
		isthatyellowkirby = guild.getMember("yellow_kirby","paladdin");
		assertNull(isthatyellowkirby);
		
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member yellow_kirby = new OrdinaryMember("yellow_kirby","paladdin");
		guild.addMember(guildMaster, yellow_kirby);
		Member isthatyellowkirby = guild.getMember("yellow_kirby","paladdin");
		assertNotNull(isthatyellowkirby);
		assertEquals(isthatyellowkirby.getName(),"yellow_kirby");
		assertEquals(isthatyellowkirby.getRole(),"paladdin");
    }
}
