package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(regularUpgrade.getName(),"Sword");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(regularUpgrade.getDescription(),"a regular sword");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(26 <= regularUpgrade.getWeaponValue());
        assertTrue(regularUpgrade.getWeaponValue()<=30);
    }

}
