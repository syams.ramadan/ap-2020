package id.ac.ui.cs.advprog.tutorial5.soul.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import id.ac.ui.cs.advprog.tutorial5.soul.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.soul.service.SoulService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import java.util.*;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

	@Autowired
	private final SoulService soulService;
	
	public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return new ResponseEntity<List<Soul>>(soulService.findAll(),HttpStatus.OK);
		//return "redirect:/soul/";
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity create(@RequestBody Soul soul) {
        soulService.register(soul);
		return new ResponseEntity<>(HttpStatus.OK);
		//return "redirect:/soul/";
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Soul>> findById(@PathVariable Long id) {
        return new ResponseEntity<Optional<Soul>>(soulService.findSoul(id),HttpStatus.OK);
		//return "redirect:/soul/";
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        return new ResponseEntity<Soul>(soulService.rewrite(id,soul),HttpStatus.OK);
		//return "redirect:/soul/";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        soulService.erase(id);
		return new ResponseEntity<>(HttpStatus.OK);
		//return "redirect:/soul/";
    }
}
