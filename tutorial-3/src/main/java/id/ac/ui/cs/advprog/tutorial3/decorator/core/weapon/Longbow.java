package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    public Longbow(){
		this.weaponName = "Longbow";
		this.weaponDescription = "long bow";
		this.weaponValue = 15;
	}
}
