package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



public class EnhancerDecoratorTest {

    Weapon weapon1 = new Gun();
    Weapon weapon2 = new Longbow();
    Weapon weapon3 = new Sword();
    Weapon weapon4 = new Shield();
    Weapon weapon5 = new Gun();
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);
		assertEquals("a chaotic scary gun", weapon1.getDescription());
        assertEquals("a magical long bow", weapon2.getDescription());
        assertEquals("a regular sword", weapon3.getDescription());
        assertEquals("a raw shield", weapon4.getDescription());
        assertEquals("a unique scary gun", weapon5.getDescription());
    }

}
