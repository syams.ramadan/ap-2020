package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    String name;
	String role;
	List<Member> bawahanlist = new ArrayList<>();
	
	public PremiumMember(String name, String role){
		this.name = name;
		this.role = role;
	}
	public String getName(){ 
		return this.name ;
	}
	public String getRole(){
		return this.role;
	}
	public void addChildMember(Member member){
		if(role.equals("Master") == false && bawahanlist.size()>=3){
			for(Member m : bawahanlist){
				m.addChildMember(member);
			}
		}
		else if(bawahanlist.size()<3 || role.equals("Master") == true) bawahanlist.add(member); 
	}
	public void removeChildMember(Member member){ 
		bawahanlist.remove(member);
	}
	public List<Member> getChildMembers(){
		return bawahanlist;
	}
}
