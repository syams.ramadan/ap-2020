package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
	public HolyGrail hg;
	
	@BeforeEach
	public void setUp(){
		hg = new HolyGrail();
		hg.makeAWish("mau makan laper");
	}
	@Test
	public void testGetWish(){
		assertEquals(hg.getHolyWish().getWish(),"mau makan laper");
	}
	@Test
	public void testSetWish(){
		hg.makeAWish("udah makan jadi ga laper");
		assertEquals("udah makan jadi ga laper",hg.getHolyWish().getWish());
	}
}
