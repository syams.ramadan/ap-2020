package id.ac.ui.cs.advprog.tutorial5.soul.service;
import id.ac.ui.cs.advprog.tutorial5.soul.core.Soul;
import java.util.*;


public interface SoulService {
	public List<Soul> findAll();
	public Optional<Soul> findSoul(Long id);
	public void erase(Long id); //delete
	public Soul rewrite(Long id, Soul soul); //update
	public Soul register(Soul soul); //create
}
