package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals(member.getName(),"Wati");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(),"Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        Member blue_kirby = new OrdinaryMember("blue_kirby", "Mage");
        member.addChildMember(blue_kirby);
        List<Member> memberlist = member.getChildMembers();
        assertEquals(memberlist.size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member blue_kirby = new OrdinaryMember("blue_kirby", "Mage");
        member.addChildMember(blue_kirby);
        member.removeChildMember(blue_kirby);
		List<Member> memberlist = member.getChildMembers();
        assertEquals(memberlist.size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        for(int i =0;i<4;i++) {
            Member kirby = new OrdinaryMember("kirby warna "+i, "test");
            member.addChildMember(kirby);
        }
        List<Member> members = member.getChildMembers();
        assertEquals(members.size(), 3);

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member pink_kirby = new PremiumMember("pink_kirby", "Master");

        for(int i =0;i<4;i++) {
            Member kirby = new OrdinaryMember("kirby warna "+i, "test");
            pink_kirby.addChildMember(kirby);
        }
        List<Member> members = pink_kirby.getChildMembers();
        assertEquals(members.size(), 4);

    }
}
