package id.ac.ui.cs.advprog.tutorial5.soul.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "soul")
public class Soul {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	 
	@Column(name = "name")
	private String name;
	
	public Soul(long id, String name, int age, String gender, String occupation){
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.occupation = occupation;
	}
	
	public Soul(){
		
	}
	
	@Column(name = "age")
	private int age;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "occupation")
	private String occupation;
	
	public String getName(){return name;}
	public int getAge(){return age;}
	public long getId(){return id;}
	public String getGender(){return gender;}
	public String getOccupation(){return occupation;}
	public void setName(String name){this.name = name;}
	public void setAge(int age){this.age = age;}
	public void setGender(String gender){ this.gender=gender;}
	public void setOccupation(String occupation){ this.occupation=occupation;}
	public Soul get(){return this;}
}
