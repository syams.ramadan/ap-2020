package id.ac.ui.cs.advprog.tutorial5.soul.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import id.ac.ui.cs.advprog.tutorial5.soul.core.Soul;

public interface SoulRepository extends JpaRepository<Soul, Long> {
}
