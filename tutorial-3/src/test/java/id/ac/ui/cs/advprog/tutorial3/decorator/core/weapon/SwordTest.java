package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(weapon.getName(),"Sword");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(weapon.getDescription(),"sword");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(25, weapon.getWeaponValue());
    }
}