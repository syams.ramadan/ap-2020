package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(weapon.getName(),"Gun");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(weapon.getDescription(),"scary gun");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(20, weapon.getWeaponValue());
    }
}
