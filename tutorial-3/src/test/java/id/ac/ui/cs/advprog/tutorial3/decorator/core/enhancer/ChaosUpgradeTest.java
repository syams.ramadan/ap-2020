package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(chaosUpgrade.getName(), "Gun");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        assertEquals(chaosUpgrade.getDescription(),"a chaotic scary gun");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(70 <= chaosUpgrade.getWeaponValue());
        assertTrue(chaosUpgrade.getWeaponValue() <= 75);
    }

}
