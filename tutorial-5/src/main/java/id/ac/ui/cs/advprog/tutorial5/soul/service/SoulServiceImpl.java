// TODO: Import service bean
package id.ac.ui.cs.advprog.tutorial5.soul.service;
import id.ac.ui.cs.advprog.tutorial5.soul.core.Soul;
import java.util.*;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.jpa.repository.JpaRepository;
import id.ac.ui.cs.advprog.tutorial5.soul.repository.SoulRepository;

@Service
public class SoulServiceImpl implements SoulService {
	
	@Autowired
	SoulRepository sr;
	
	public SoulServiceImpl(SoulRepository soulRepository) {
        this.sr = soulRepository;
    }
	
	@Override
	public List<Soul> findAll(){return sr.findAll();}
	
	@Override
	public Optional<Soul> findSoul(Long id){return sr.findById(id);}
	
	@Override
	public void erase(Long id){sr.deleteById(id);
	}
	
	@Override
	public Soul register(Soul soul){
		return sr.save(soul);
	}
	
	public Soul rewrite(Long id, Soul soul){
        Soul soula = findSoul(id).get();
        soula.setName(soul.getName());
        soula.setAge(soul.getAge());
        soula.setGender(soul.getGender());
        soula.setOccupation(soul.getOccupation());
		sr.save(soula);
        return soula;
	}
	// TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service
}
