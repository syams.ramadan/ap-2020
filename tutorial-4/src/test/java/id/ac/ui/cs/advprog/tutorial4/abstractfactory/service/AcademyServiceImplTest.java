package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository = new AcademyRepository();

    @InjectMocks
    private AcademyService academyService;
	
	@Test
	public void testProduceKnight(){
        academyService = new AcademyServiceImpl(academyRepository);
        assertTrue(academyService.getKnightAcademies().size() != 0);
        academyService.produceKnight("Lordan Academy", "majestic");
    }
}
