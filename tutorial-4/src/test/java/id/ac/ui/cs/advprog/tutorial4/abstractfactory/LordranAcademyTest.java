package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        lordranAcademy = new LordranAcademy();
		majesticKnight = lordranAcademy.getKnight("majestic");
		metalClusterKnight =lordranAcademy.getKnight("metal cluster");
		syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals(majesticKnight.getName(),"Majestic Knight");
        assertEquals(metalClusterKnight.getName(),"Metal Cluster Knight");
        assertEquals(syntheticKnight.getName(),"Synthetic Knight");
    }

    @Test
    public void checkKnightDescriptions() {
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertNull(majesticKnight.getSkill());
		assertTrue(majesticKnight.getArmor() instanceof ShiningArmor);
		assertTrue(majesticKnight.getWeapon() instanceof ShiningBuster);
		
		assertNotNull(metalClusterKnight.getArmor());
		assertNotNull(metalClusterKnight.getSkill());
		assertNull(metalClusterKnight.getWeapon());
		assertTrue(metalClusterKnight.getArmor() instanceof ShiningArmor);
		assertTrue(metalClusterKnight.getSkill() instanceof ShiningForce);
		
		assertNotNull(syntheticKnight.getWeapon());
		assertNotNull(syntheticKnight.getSkill());
		assertNull(syntheticKnight.getArmor());
		assertTrue(syntheticKnight.getWeapon() instanceof ShiningBuster);
		assertTrue(syntheticKnight.getSkill() instanceof ShiningForce);
    }
}
